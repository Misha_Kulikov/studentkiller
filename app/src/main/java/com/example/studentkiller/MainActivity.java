package com.example.studentkiller;



import androidx.appcompat.app.AppCompatActivity;

import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Integer counter = 0;
    TextView counterView;
    final String TAG = "StartActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate");
        counterView = findViewById(R.id.txt_counter);
    }

    //Вызывается, когда Активность стала видимой
    @Override
    public void onStart(){
        super.onStart();
        //Проделать необходимые действия для Активности, видимой на экране
        Log.d(TAG, "onStart");
    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    protected void onPause(){
        super.onPause();
        Log.d(TAG, "onPause");
    }

    //Вызывается перед выходом из Активного состояния, позволяя сохранить
    //состояние в объекте savedInstanceState
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt("counter", counter);
    }
    @Override
    public void onRestoreInstanceState(Bundle saveInstanceState){
        super.onRestoreInstanceState(saveInstanceState);
        if(saveInstanceState !=null && saveInstanceState.containsKey("counter")){
            counter = saveInstanceState.getInt("counter");
            counterView.setText(String.valueOf(counter));
        }
    }

    @Override
    protected void onStop(){
        super.onStop();
        Log.d(TAG, "onStop");
    }

    // Вызывается перед тем, как Активность снова становится видимой

    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    public void onClickBtnAddStudents(View view){
        counter++;
        counterView.setText(String.valueOf(counter));
    }
}